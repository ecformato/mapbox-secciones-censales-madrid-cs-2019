import { text } from 'd3-request';
import { dsvFormat } from 'd3-dsv';
import '../css/main.scss';

/* VARIABLES DE COLORES (primer color = + claro; segundo color = + oscuro) */
let cs_aux = ['#FEDDD3', '#ED946C', '#F25B22', '#AA3205']

mapboxgl.accessToken = 'pk.eyJ1IjoiZGF0b3MtZWxjb25maWRlbmNpYWwiLCJhIjoiY2syNHNsMGN0MWRyZDNubnkwaHFiZGw4ayJ9.eKIhDFvX3YotsAEtGihnAw'; //Cambiar

/* Ajustar ZOOM, MINZOOM Y CENTER EN FUNCIÓN DEL DISPOSITIVO */
let mapWidth = document.getElementById('mapa').clientWidth;
let zoom = mapWidth > 525 ? 8 : 7.5;
let minZoom = mapWidth > 525 ? 7 : 7;
let center = [-3.75, 40.55];
let hoveredStateId = null;

let map = new mapboxgl.Map({
    container: 'mapa',
    style: 'mapbox://styles/datos-elconfidencial/ckmowiz7t7pg517qvw94iyz3e',
    attributionControl: false,
    zoom: zoom,
    minZoom: minZoom,
    maxZoom: 16,
    center: center
});

map.addControl(
    new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl,
        language: 'ES',
        marker: false,
        placeholder: 'Busque por calle o municipio'
    })
);

/* Variable para poder navegar sobre el mapa */
let nav = new mapboxgl.NavigationControl({showCompass:false});
map.addControl(nav);

/* Inicializar variable para trabajar con popups */
let popup = new mapboxgl.Popup({
    closeButton: true,
    closeOnClick: false
});

map.scrollZoom.disable();

map.on('load', function(){
    let file = 'https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/secciones_madrid_mapbox.csv';

    text(file, function(data){
        let dsv = dsvFormat(";");
        let parseData = dsv.parse(data);

        map.addSource('secciones_censales', {
            'type': 'vector',
            'url': 'mapbox://datos-elconfidencial.aupbtynu',
            promoteId: 'CUSEC'
        });

        
        parseData.map((item) => {
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_cam-a5lgu8',
                id: +item['Cusec']
            }, {
                cs_porc: +item['% Cs'].replace(',','.')
            });
        });

        //Coropletas > Primero rellenamos con ganador
        map.addLayer({
            'id': 'secciones_censales_madrid',
            'source': 'secciones_censales',
            'source-layer': 'secciones_cam-a5lgu8',
            'type': 'fill',
            'paint': {
                'fill-color': [
                    'step',
                    ['feature-state', 'cs_porc'],
                    'orange',
                    0,
                    cs_aux[0],
                    10,
                    cs_aux[1],
                    20,
                    cs_aux[2],
                    30,
                    cs_aux[3]
                ],
                'fill-opacity': 0.9
            }      
        }, 'road-simple');

        //Límites censales
        map.addLayer({
            'id': 'secciones_censales_line',
            'source': 'secciones_censales',
            'source-layer': 'secciones_cam-a5lgu8',
            'type': 'line',
            'paint': {
                'line-color': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    'black',
                    '#fff'
                ],
                'line-width': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    1.75,
                    .01
                ]
            }      
        }, 'road-label-simple');

        //Eventos de ratón
        bind_event(popup, 'secciones_censales_madrid');   
    });
});

////// Otros elementos del DOM ////////

if(window.innerWidth < 640){
    map.dragPan.disable();
    map.on('zoom', function(){
        let zoom = map.getZoom();
        
        if(zoom > 7){
            map.dragPan.enable();
        } else {
            map.dragPan.disable();
        }
    });
}

//Uso del tooltip
function bind_event(popup, id){
    map.on('mousemove', id, function(e){
        //Primera parte
        let propiedades = e.features[0];
        map.getCanvas().style.cursor = 'pointer';
        var coordinates = e.lngLat;        
        var tooltipText = get_tooltip_text(propiedades);
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }        
        popup.setLngLat(coordinates)
            .setHTML(tooltipText)
            .addTo(map);

        //Segunda parte
        if (e.features.length > 0) {
            if (hoveredStateId) {
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_cam-a5lgu8',
                id: hoveredStateId
            }, {
                hover: false
            });
            }
            hoveredStateId = e.features[0].id;
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_cam-a5lgu8',
                id: hoveredStateId
            }, {
                hover: true
            });
        }
        map.getCanvas().style.cursor = 'pointer';
    });
    map.on('mouseleave', id, function() {
        map.getCanvas().style.cursor = '';
        popup.remove();

        if (hoveredStateId) {
        map.setFeatureState({
            source: 'secciones_censales',
            sourceLayer: 'secciones_cam-a5lgu8',
            id: hoveredStateId
        }, {
            hover: false
        });
        }
        hoveredStateId = null;
    });
}  

function get_tooltip_text(propiedades){
    let caracteristicas_fijas = propiedades.properties;
    let estado = propiedades.state;
    
    let html = '';
    html = `<p><b>${caracteristicas_fijas.NMUN}</b> (Sección: ${caracteristicas_fijas.CUSEC.substr(-5)})</p>
            <p>Porc. de voto a CS: ${estado.cs_porc}%</p>`;    
    return html; 
}